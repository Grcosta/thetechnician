// Fill out your copyright notice in the Description page of Project Settings.

#include "SelectNextPatrolPointCPP.h"
#include "AIController.h"
#include "PatrolRoute.h"
#include "BehaviorTree//BlackboardComponent.h"


EBTNodeResult::Type USelectNextPatrolPointCPP::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	//Get the patrol points from the NPC
	auto ControlledPawn = OwnerComp.GetAIOwner()->GetPawn();
	auto PatrolRouteComponent = ControlledPawn->FindComponentByClass<UPatrolRoute>();

	// Check if the Patrol Route Component is attached:
	if (!ensure(PatrolRouteComponent)) { return EBTNodeResult::Failed; }

	// Check if the patrol route is empty
	auto PatrolPoints = PatrolRouteComponent->GetPatrolPointsComp();
	if (PatrolPoints.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("A patrol guard has NO POINTS!"));
		return EBTNodeResult::Failed;
	}

	//Set Next Patrol Point
	auto BlackboardComp = OwnerComp.GetBlackboardComponent();
	auto Index = BlackboardComp->GetValueAsInt(IndexKey.SelectedKeyName);
	BlackboardComp->SetValueAsObject(NextPointKey.SelectedKeyName, PatrolPoints[Index]);
	
	//Cycle the Patrol Points Index for the next move
	auto NextIndex = (Index + 1) % PatrolPoints.Num();
	BlackboardComp->SetValueAsInt(IndexKey	.SelectedKeyName, NextIndex);
	
	
	return EBTNodeResult::Succeeded;
}

