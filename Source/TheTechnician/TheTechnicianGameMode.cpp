// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "TheTechnicianGameMode.h"
#include "TheTechnicianHUD.h"
#include "TheTechnicianCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATheTechnicianGameMode::ATheTechnicianGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATheTechnicianHUD::StaticClass();
}
